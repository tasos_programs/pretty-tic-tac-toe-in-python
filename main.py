from board import Board


def Ask_New_Game() -> bool:
    while True:
        answer = input("Game over. Would you like to play again? [Y/n]: ")
        answer = answer.lower().strip()
        if answer in ['y', "yes", '']:
            return True
        elif answer in ['n', "no"]:
            return False


def Do_Board_Full_Action(game: Board) -> None:
    game.Print_Playspace()
    print("Board full")


def Do_Win_Action(game: Board, pos: list) -> None:
    game.Print_Colored_Winner(pos)
    # since the game swaps after each turn, swap again so we get the correct player
    game.Swap_Turns()
    print(f"Player {game.player_turn} wins!")


def Check_For_Endings(game: Board) -> bool:
    win, pos = game.Check_Win()
    if win:
        Do_Win_Action(game, pos)
        return True
    
    if game.Is_Board_Full():
        Do_Board_Full_Action(game)
        return True
    
    return False 


def New_Game() -> None:
    game = Board()
    while True:
        user_input = game.Get_Input()
        game.Set_Position(user_input)
        if Check_For_Endings(game):
            return
    

def Print_Instructions() -> None:
    Board.Clear_Screen()
    print("How to play:")
    print("'x' plays first. Type a position from 1-9 and press Enter to submit.")
    print("Positions start from top left to bottom right")
    _ = input("\n\x1B[3mPress Enter to continue...\x1B[0m")


def main() -> None:
    Print_Instructions()
    while True:
        try:
            New_Game()
            if not Ask_New_Game():
                break
        except KeyboardInterrupt:
            return


if __name__ == "__main__":
    main()
