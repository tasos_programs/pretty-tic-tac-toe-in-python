# Pretty Tic Tac Toe in Python
A Pretty, Object Oriented version of the classic game Tic Tac Toe written in Python
# Requirements
You need to have:

-[Python](python.org/downloads/) 

-The ``termcolor`` pip module

You can get ``termcolor`` by typing ``pip install termcolor`` in your terminal

Open the game by opening a terminal window in the directory you downloaded the files, and type ``python main.py``


# Screenshots
![Start screen](https://gitlab.com/tasos_programs/pretty-tic-tac-toe-in-python/-/raw/main/Screenshots/how_to_play.png)

![Empty board](https://gitlab.com/tasos_programs/pretty-tic-tac-toe-in-python/-/raw/main/Screenshots/empty.png)

![Win](https://gitlab.com/tasos_programs/pretty-tic-tac-toe-in-python/-/raw/main/Screenshots/win.png)
