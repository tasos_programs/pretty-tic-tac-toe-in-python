import os
from termcolor import colored


class Board:
    def __init__(self):
        self.playspace = '-' * 9
        self.player_turn = 'x'

    @staticmethod
    def Clear_Screen():
        os.system('cls' if os.name == 'nt' else 'clear')

    def Swap_Turns(self):
        self.player_turn = 'x' if self.player_turn == 'o' else 'o'

    @staticmethod
    def Correct_Color(index: int, positions: list[str]) -> str:
        return "green" if index in positions else "white"

    def Print_Colored(self, character: str, index: int, positions: list) -> str:
        result = ((colored(
            character, self.Correct_Color(index, positions))))
        return result

    def Print_Playspace(self, positions=None) -> None:
        self.Clear_Screen()
        color = "green" if positions is not None else "white"
        positions = [] if positions is None else positions  # Ensures working code below

        print(",___________,")
        for index, character in enumerate(self.playspace):
            if index in [2, 5, 9]:
                print('', self.Print_Colored(
                    character, index, positions), '|\n|───|───|───|')
            else:
                if index == 8:  # WHY?
                    print(
                        '', self.Print_Colored(character, index, positions), '|', end='')
                elif index in [0, 3, 6]:
                    print(
                        '|', self.Print_Colored(character, index, positions), '', end='')
                else:
                    print('|', self.Print_Colored(
                        character, index, positions), '|', end='')
        print("\n`¯¯¯¯¯¯¯¯¯¯¯`")

    def Verify_Input(self, user_input: str) -> bool:
        if user_input.isdigit():
            return True if self.Check_Correct_Positions(int(user_input)) else False
        return False

    def Check_Correct_Positions(self, user_input: int) -> bool:
        if user_input > 0 and user_input < 10:
            if self.playspace[user_input - 1] == '-':
                return True
        return False

    def Get_Input(self) -> int:
        self.Print_Playspace()
        while True:
            user_input = input(
                f"[Player {self.player_turn}]: Choose a position [1-9]: ")
            user_input = user_input.strip()
            if self.Verify_Input(user_input):
                return int(user_input) - 1
            print("Invalid input")

    def Set_Position(self, position: int) -> None:
        # Since py str are immutable
        arr_of_playspace = list(self.playspace)
        arr_of_playspace[position] = self.player_turn
        self.playspace = ''.join(arr_of_playspace)
        # swap player
        self.Swap_Turns()

    def Is_Board_Full(self):
        return False if any(c == '-' for c in self.playspace) else True

    def Get_Three_Positions(self, item: list) -> str:
        result = []
        for i in range(3):
            result.append(self.playspace[item[i]])      
        return result  
    
    @staticmethod
    def Check_For_Three_Same(result: list) -> bool:
        return True if ''.join(result) in ["xxx", "ooo"] else False

    def Check_Each_Win(self, position: list[list]) -> bool and list:
        for index, item in enumerate(position):
            result = self.Get_Three_Positions(item)
            if self.Check_For_Three_Same(result):
                return True, position[index]
        return False, None

    def Check_Win(self) -> bool and list:
        vertical = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
        horizontal = [[0, 3, 6], [1, 4, 7], [2, 5, 8]]
        diagonal = [[0, 4, 8], [2, 4, 6]]

        all_conditions = [vertical, horizontal, diagonal]
        for condition in all_conditions:
            (win, pos) = self.Check_Each_Win(condition)
            if win:
                return True, pos
        return False, None

    def Print_Colored_Winner(self, win_position=None) -> None:
        # Check the wins again, this time getting the positions of the win.
        self.Print_Playspace(win_position)
